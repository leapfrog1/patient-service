package com.leapfrog.hms.props;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("pms.aws")
@Data
public class AWSProperties {
    private Credentials credentials;
    private Region region;

    @Data
    public static class Credentials {
        private String accessKeyId;
        private String secretKey;
    }

    @Data
    public static class Region {
        private String name;
    }
}