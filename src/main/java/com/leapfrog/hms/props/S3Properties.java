package com.leapfrog.hms.props;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("pms.aws.s3")
@Data
public class S3Properties {
    private S3Bucket bucket;

    @Data
    public static class S3Bucket {
        private String name;
    }
}
