package com.leapfrog.hms.config;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.leapfrog.hms.props.AWSProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class AWSConfig {
    private final AWSProperties awsProperties;
    @Bean
    public AmazonS3 s3() {
        AWSCredentials awsCredentials =
                new BasicAWSCredentials(awsProperties.getCredentials().getAccessKeyId(), awsProperties.getCredentials().getSecretKey());
        return AmazonS3ClientBuilder
                .standard()
                .withRegion(awsProperties.getRegion().getName())
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .build();
    }
}
