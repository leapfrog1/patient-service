package com.leapfrog.hms.service;

import com.leapfrog.hms.dto.PagedData;
import com.leapfrog.hms.dto.PatientDTO;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface PatientService {
    PatientDTO add(MultipartFile photo, PatientDTO patient) throws IOException;
    PatientDTO getBy(long id);
    PagedData<PatientDTO> getPageableData(Pageable pageable, String query);
    PatientDTO updateSpecialAttention(String id, boolean value);
    void delete(String id);
}
