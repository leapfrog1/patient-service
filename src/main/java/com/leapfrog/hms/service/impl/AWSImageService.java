package com.leapfrog.hms.service.impl;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.leapfrog.hms.dto.AWSImageMeta;
import com.leapfrog.hms.service.ImageService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AWSImageService implements ImageService<AWSImageMeta> {

    private final AmazonS3 amazonS3;

    @Override
    public void upload(AWSImageMeta image) throws AmazonServiceException {
        amazonS3.putObject(image.getPath(), image.getFileName(), image.getInputStream(), image.getMetadata());
    }
}
