package com.leapfrog.hms.service.impl;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.leapfrog.hms.dto.AWSImageMeta;
import com.leapfrog.hms.dto.PagedData;
import com.leapfrog.hms.dto.PatientDTO;
import com.leapfrog.hms.entity.Patient;
import com.leapfrog.hms.exceptions.NoDataException;
import com.leapfrog.hms.mapper.PatientMapper;
import com.leapfrog.hms.props.S3Properties;
import com.leapfrog.hms.repository.PatientRepository;
import com.leapfrog.hms.service.ImageService;
import com.leapfrog.hms.service.PatientService;
import com.leapfrog.hms.utils.AWSUtil;
import com.leapfrog.hms.utils.PageableData;
import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.UUID;

import static com.leapfrog.hms.constant.ErrorMessageConstants.NO_SUCH_PATIENT;
import static com.leapfrog.hms.utils.ImageUtil.validateImage;
import static org.apache.http.entity.ContentType.*;

@Service
@Transactional
@RequiredArgsConstructor
public class PatientServiceImpl implements PatientService {
    private final PatientRepository repository;
    private final PatientMapper mapper = Mappers.getMapper(PatientMapper.class);
    private final ImageService<AWSImageMeta> imageService;
    private final S3Properties properties;
    private final AmazonS3 amazonS3;

    @Override
    public PatientDTO add(MultipartFile photo, PatientDTO patient) throws IOException {
        Patient persistableData = mapper.toEntity(patient);
        if (photo != null) {
            validateImage(photo, IMAGE_BMP, IMAGE_PNG, IMAGE_JPEG);
            AWSImageMeta imageMeta = buildImageMeta(photo);
            imageService.upload(imageMeta);
            persistableData.setImagePath(imageMeta.getPath() + "/" + imageMeta.getFileName());
        }
        persistableData.setStatus(true);
        return mapper.toDTO(repository.save(persistableData));
    }

    @Override
    public PatientDTO updateSpecialAttention(String id, boolean value) {
        Patient patient = repository.findById(Long.parseLong(id)).orElseThrow(this::noSuchPatient);
        patient.setSpecialAttention(value);
        return mapper.toDTO(patient);
    }

    @Override
    public PagedData<PatientDTO> getPageableData(Pageable pageable, String query) {
        PagedData<PatientDTO> pagedData = new PageableData<PatientDTO, Patient, Long>()
                .setMapper(mapper)
                .setRepository(repository)
                .get(() -> repository.findAllIdsBy(pageable, true, query.replaceAll("\\s", "")));
        pagedData.getData().forEach(this::setTempImagePath);
        return pagedData;
    }

    @Override
    public PatientDTO getBy(long id) {
        PatientDTO patientDTO = mapper.toDTO(repository.findById(id).orElseThrow(this::noSuchPatient));
        setTempImagePath(patientDTO);
        return patientDTO;
    }

    @Override
    public void delete(String id) {
        repository.deleteById(Long.valueOf(id));
    }

    private NoDataException noSuchPatient() {
        return new NoDataException(NO_SUCH_PATIENT);
    }

    private void setTempImagePath(PatientDTO patient) {
        if (patient.getImagePath() != null) {
            String bucketName = properties.getBucket().getName();
            String key = patient.getImagePath().replaceAll(bucketName + "/", "");
            try {
                patient.setTempImagePath(AWSUtil.generateUrl(amazonS3, bucketName, key, 1));
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    private AWSImageMeta buildImageMeta(MultipartFile image) throws IOException {
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.addUserMetadata("Content-Type", image.getContentType());
        objectMetadata.addUserMetadata("Content-Length", String.valueOf(image.getSize()));
        return AWSImageMeta.builder()
                .fileName(String.format("%s", image.getOriginalFilename()))
                .path(String.format("%s/%s", properties.getBucket().getName(), UUID.randomUUID()))
                .inputStream(image.getInputStream())
                .metadata(objectMetadata)
                .build();
    }
}
