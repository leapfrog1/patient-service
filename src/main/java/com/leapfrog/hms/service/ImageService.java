package com.leapfrog.hms.service;

import com.amazonaws.AmazonServiceException;
import com.leapfrog.hms.dto.ImageMeta;

public interface ImageService<T extends ImageMeta> {
    void upload(T image) throws AmazonServiceException;
}
