package com.leapfrog.hms.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.InputStream;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ImageMeta {
    String path;
    String fileName;
    InputStream inputStream;
}
