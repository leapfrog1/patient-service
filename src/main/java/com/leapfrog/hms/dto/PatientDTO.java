package com.leapfrog.hms.dto;

import com.leapfrog.hms.dto.base.Identity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.web.multipart.MultipartFile;

@EqualsAndHashCode(callSuper = true)
@Data
public class PatientDTO extends Identity<Long> {
    private String firstName;
    private String middleName;
    private String lastName;
    private String emailAddress;
    private String mobileNumber;
    private String dob;
    private boolean specialAttention;
    private String imagePath;
    private String tempImagePath;
}
