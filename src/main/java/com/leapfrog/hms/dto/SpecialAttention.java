package com.leapfrog.hms.dto;

import lombok.Data;

@Data
public class SpecialAttention {
    private boolean specialAttention;
}
