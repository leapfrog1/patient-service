package com.leapfrog.hms.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class MessageResponseDTO extends HttpMessageResponse {

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageResponseDTO messageResponse = (MessageResponseDTO) o;
        return Objects.equals(getCode(), messageResponse.getCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCode());
    }
}
