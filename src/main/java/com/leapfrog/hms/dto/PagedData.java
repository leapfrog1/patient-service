package com.leapfrog.hms.dto;

import com.leapfrog.hms.dto.base.PageableCount;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PagedData<D> extends PageableCount {
    private int totalPages;
    private List<D> data;

    public PagedData(long count, int totalPages, List<D> data) {
        super(count);
        this.totalPages = totalPages;
        this.data = data;
    }
}
