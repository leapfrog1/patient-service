package com.leapfrog.hms.dto;

import com.amazonaws.services.s3.model.ObjectMetadata;
import lombok.*;

import java.io.InputStream;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class AWSImageMeta extends ImageMeta{
    ObjectMetadata metadata;

    @Builder
    public AWSImageMeta(String path, String fileName, InputStream inputStream, ObjectMetadata metadata) {
        super(path, fileName, inputStream);
        this.metadata = metadata;
    }
}
