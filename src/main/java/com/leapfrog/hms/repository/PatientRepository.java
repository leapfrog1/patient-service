package com.leapfrog.hms.repository;

import com.leapfrog.hms.entity.Patient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;


public interface PatientRepository extends CustomPageableRepository<Patient, Long> {

    @Query(Queries.PATIENT_LIST)
    Page<Long> findAllIdsBy(Pageable pageable, boolean status, String query);

    final class Queries {

        public static final String PATIENT_LIST = "select t.id from Patient t where t.status = :status " +
                " and " +
                "(" +
                " concat(t.firstName, coalesce(t.middleName, '') ,t.lastName) like %:query% " +
                " or " +
                "t.emailAddress like %:query% " +
                " or " +
                "t.mobileNumber like %:query% " +
                ")";

        private Queries() {
        }
    }
}
