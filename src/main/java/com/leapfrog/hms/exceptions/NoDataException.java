package com.leapfrog.hms.exceptions;

public class NoDataException extends RuntimeException {

    public NoDataException(String messageId) {
        super(messageId);
    }
}
