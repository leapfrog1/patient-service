package com.leapfrog.hms.utils;

import com.leapfrog.hms.exceptions.InvalidImageException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.http.entity.ContentType;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.stream.Collectors;

import static com.leapfrog.hms.constant.ErrorMessageConstants.INVALID_IMAGE;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ImageUtil {

    public static void validateImage(MultipartFile file, ContentType... imageTypes) {
        if (file == null) {
            return;
        }
        boolean isValid = Arrays.stream(imageTypes).map(ContentType::getMimeType)
                .collect(Collectors.toList())
                .contains(file.getContentType());
        if (!isValid) {
            throw new InvalidImageException(INVALID_IMAGE);

        }
    }
}
