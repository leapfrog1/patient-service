package com.leapfrog.hms.utils;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.net.URISyntaxException;
import java.util.Date;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class AWSUtil {
    public static String generateUrl(AmazonS3 s3Client, String bucketName, String objectKey, int expirationMinute) throws URISyntaxException {
        long millisNow = System.currentTimeMillis();
        long expirationMillis = millisNow + 1000L * 60 *  expirationMinute;
        Date expiration = new java.util.Date(expirationMillis);
        GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, objectKey);
        generatePresignedUrlRequest.setMethod(HttpMethod.GET);
        generatePresignedUrlRequest.setExpiration(expiration);
        return s3Client.generatePresignedUrl(generatePresignedUrlRequest).toURI().toString();
    }
}
