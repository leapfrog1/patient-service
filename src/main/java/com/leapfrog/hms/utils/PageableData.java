package com.leapfrog.hms.utils;

import com.leapfrog.hms.dto.PagedData;
import com.leapfrog.hms.mapper.base.BaseMapper;
import com.leapfrog.hms.repository.CustomPageableRepository;
import org.springframework.data.domain.Page;

import java.util.function.Supplier;

public class PageableData<D, E, ID> extends PageableDataDelicate<D, E, ID, PagedData<D>> {

    public PagedData<D> get(Supplier<Page<ID>> ids) {
        return super.getData(ids, (pageIds, data) -> new PagedData<>(pageIds.getTotalElements(), pageIds.getTotalPages(), data));
    }

    @Override
    public PageableData<D, E, ID> setMapper(BaseMapper<D, E> baseMapper) {
        return (PageableData<D, E, ID>) super.setMapper(baseMapper)  ;
    }

    @Override
    public PageableData<D, E, ID> setRepository(CustomPageableRepository<E, ID> repository) {
        return (PageableData<D, E, ID>) super.setRepository(repository);
    }
}
