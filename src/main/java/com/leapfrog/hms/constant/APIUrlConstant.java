package com.leapfrog.hms.constant;

public final class APIUrlConstant {
    private APIUrlConstant() {
    }

    public static final String BASE_API_V1 = "api/v1";
    public static final String PATIENTS = "/patients";
    public static final String ID = "/{id}";
    public static final String PATIENTS_FULL = BASE_API_V1 + PATIENTS;

}
