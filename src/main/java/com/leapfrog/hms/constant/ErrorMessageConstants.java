package com.leapfrog.hms.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ErrorMessageConstants {

    public final static String INVALID_PARAMETER = "Invalid Request Parameter";
    public final static String NOT_UNIQUE = " has already been added";
    public final static String NO_SUCH_PATIENT = "There is no such patient";
    public final static String INVALID_IMAGE = "Please upload valid image";
}
