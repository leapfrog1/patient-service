package com.leapfrog.hms.mapper;

import com.leapfrog.hms.dto.PatientDTO;
import com.leapfrog.hms.entity.Patient;
import com.leapfrog.hms.mapper.base.BaseMapper;
import org.mapstruct.Mapper;

@Mapper
public interface PatientMapper extends BaseMapper<PatientDTO, Patient> {
}
