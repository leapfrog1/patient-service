package com.leapfrog.hms.controller;

import com.leapfrog.hms.constant.ConstraintName;
import com.leapfrog.hms.constant.ErrorMessageConstants;
import com.leapfrog.hms.dto.DataBindErrorResponse;
import com.leapfrog.hms.dto.Error;
import com.leapfrog.hms.dto.HttpErrorResponse;
import com.leapfrog.hms.dto.HttpMessageResponse;
import com.leapfrog.hms.exceptions.DataConflictException;
import com.leapfrog.hms.exceptions.NoDataException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static com.leapfrog.hms.utils.ExceptionMessageParser.getFieldName;

@RestControllerAdvice
@RequiredArgsConstructor
public class ExceptionController {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<HttpErrorResponse> methodArgumentException(MethodArgumentNotValidException ex) {
        final HttpStatus badRequest = HttpStatus.BAD_REQUEST;
        ex.printStackTrace();
        return ResponseEntity.badRequest().body(
                new DataBindErrorResponse(badRequest.value(),
                        ErrorMessageConstants.INVALID_PARAMETER,
                        badRequest,
                        mapToDataBind(ex.getBindingResult()))
        );
    }

    @ExceptionHandler(NoDataException.class)
    public ResponseEntity<HttpErrorResponse> dataNotFoundException(final NoDataException ex) {
        final HttpStatus badRequest = HttpStatus.BAD_REQUEST;
        ex.printStackTrace();
        return ResponseEntity.badRequest().body(new HttpErrorResponse(badRequest.value(), ex.getMessage(), badRequest));
    }

    @ExceptionHandler(DataConflictException.class)
    public ResponseEntity<HttpErrorResponse> dataConflictException(final DataConflictException ex) {
        String message = getFieldName(ex.getMessage(), ConstraintName.DUPLICATE, 0, 2) + ErrorMessageConstants.NOT_UNIQUE;
        final HttpStatus conflict = HttpStatus.CONFLICT;
        ex.printStackTrace();
        return ResponseEntity.status(conflict).body(new HttpErrorResponse(conflict.value(), message, conflict));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<HttpErrorResponse> exception(final Exception ex, HttpServletRequest request) {
        final HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        ex.printStackTrace();
        return ResponseEntity.internalServerError().body(new HttpErrorResponse(status.value(), ex.getMessage(), status));
    }


    private Set<Error> mapToDataBind(BindingResult bindingResult) {
        return bindingResult.getFieldErrors().stream()
                .map(fieldError -> {
                    Set<HttpMessageResponse> errorMessages = new HashSet<>();
                    errorMessages.add(new HttpMessageResponse(HttpStatus.BAD_REQUEST.value(), fieldError.getDefaultMessage()));
                    return new Error(fieldError.getField(), fieldError.getRejectedValue(), errorMessages);
                }).collect(Collectors.toSet());
    }
}
