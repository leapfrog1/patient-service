package com.leapfrog.hms.controller;

import com.leapfrog.hms.dto.*;
import com.leapfrog.hms.entity.Patient;
import com.leapfrog.hms.service.PatientService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static com.leapfrog.hms.constant.APIUrlConstant.ID;
import static com.leapfrog.hms.constant.APIUrlConstant.PATIENTS_FULL;

@RestController
@RequestMapping(PATIENTS_FULL)
@RequiredArgsConstructor
public class PatientController {

    private final PatientService patientService;

    @PostMapping(
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<PatientDTO> add(@RequestPart(value = "photo", required = false) MultipartFile photo, @RequestPart("patient") PatientDTO patient) throws IOException {
        return ResponseEntity.ok(patientService.add(photo, patient));
    }

    @GetMapping(ID)
    public ResponseEntity<PatientDTO> getBy(@PathVariable String id) {
        return ResponseEntity.ok(patientService.getBy(Long.parseLong(id)));
    }

    @GetMapping
    public ResponseEntity<PagedData<PatientDTO>> getPageableData(Pageable pageable, @RequestParam String query) {
        return ResponseEntity.ok(patientService.getPageableData(pageable, query));
    }

    @PatchMapping(ID)
    public ResponseEntity<PatientDTO> updateSpecialAttention(@PathVariable String id, @RequestBody SpecialAttention specialAttention) {
        return ResponseEntity.ok(patientService.updateSpecialAttention(id, specialAttention.isSpecialAttention()));
    }

    @DeleteMapping(ID)
    public ResponseEntity<HttpMessageResponse> deletePatient(@PathVariable String id) {
        patientService.delete(id);
        return ResponseEntity.ok(new HttpMessageResponse(HttpStatus.OK.value(), "Delete Successfully"));
    }
}
