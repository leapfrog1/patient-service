package com.leapfrog.hms.entity;


import com.leapfrog.hms.entity.base.Identity;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "tbl_patient")
@Getter
@Setter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@AllArgsConstructor
@NoArgsConstructor
public class Patient extends Identity<Long> {
    @Column(nullable = false)
    private String firstName;
    @Column
    private String middleName;

    @Column
    private String lastName;

    @Column
    @EqualsAndHashCode.Include
    private String emailAddress;

    @Column
    @EqualsAndHashCode.Include
    private String mobileNumber;

    @Column
    private LocalDate dob;

    @Column
    private boolean specialAttention;

    @Column
    private String imagePath;

    @Column
    private boolean status;

}
